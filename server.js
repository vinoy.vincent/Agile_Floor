var express=require('express');
var app=express();
var session=require('express-session');
var mongoose=require('mongoose');
var http = require('http');
var path=require('path');
var fs=require("fs");
var mongojs=require('mongojs');
var bodyParser=require("body-parser");
var jwt=require("jwt-simple");
app.set('jwtSecret',"secret");
var Grid=require("gridfs-stream");
var ObjectId = require('mongoose').Types.ObjectId; 
var random=require("random-js");
var dateformat=require("dateformat");




var log4js = require('log4js');
log4js.clearAppenders();
log4js.configure({
  appenders: [
    { type: 'console' ,  category: 'agile_floor'},
    { type: 'file', filename: 'public/log/logs.log', category: 'agile' }
  ]
});

var logger = log4js.getLogger('agile');
logger.setLevel('INFO');




process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var db=mongoose.connect('mongodb://localhost/agile',['order', 'users']);

Grid.mongo=mongoose.mongo;
var conn=mongoose.connection;
var gfs=Grid(conn.db);

var orderSchema=mongoose.Schema({
	_id : String,
	orderId : String,
	lineDelivery : [{
		deliveryNo : String,
		item : [{
			itemId : {
				name : String,
				brand : String,
				salePrice : Number,
				quantity : Number,
				priceWithDiscount : Number,
				status : [{
						status : String,
						time : Date,
						place : String
					}]
			}
		}]
	}],
	totalPrice : Number,
	user : String,
	date : Date,
	address : String,
	paymentMode : String
},{collection:'order'});


var usersSchema=mongoose.Schema({
	_id : String,
	email: String,
	password : String,
	mobileNo : Number,
	address : String,
	name : String
},{collection:'users'});

var order=mongoose.model('order',orderSchema);
var users=mongoose.model('users',usersSchema);



app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(__dirname + '/uploads'));
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist/'));
app.use('/angular', express.static(__dirname + '/node_modules/angular/'));
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/angular-route', express.static(__dirname + '/node_modules/angular-route/'));
app.use('/angular-local-storage', express.static(__dirname + '/node_modules/angular-local-storage/dist/'));
app.use('/font-awesome', express.static(__dirname + '/node_modules/font-awesome/'));


app.listen(3000);
var secret="start";
console.log("server is running on 3000");
app.use(bodyParser.urlencoded({'extended':true}));
app.use(bodyParser.json());

var token=[];
var loginCount=0;
var secrets=[];

app.post('/login',function(req,res){
	var existingSess=false;
	var existingToken="";
	users.findOne({_id:req.body.email,password:req.body.password},function(err,user){
		if(err) {
			logger.error("Error occurred when "+req.body.email+" is try to login "+err);
			res.send(err);
		}
		else if(user!=null)
		{
			var payload = { email: user._id, password: user.password};
			for(var i=0;i<token.length;i++){
				if(token[i]!=undefined && secrets[i]!=undefined){
					if(jwt.decode(token[i], secrets[i]).email==req.body.email)
					{
						token.splice(i,1);
						secrets.splice(i,1);
						break;
					}
					
				}
			}
			secret = 'agilesecret'+Math.random();
			var newToken=jwt.encode(payload, secret);
			token[loginCount]=newToken;
			secrets[loginCount]=secret;
			loginCount++;
			logger.info(req.body.email+" is logged in successfully");
			res.json({"user":user,"token":token[loginCount-1]});
			

		}
		else{
			if (req.body.email!=undefined) {
				logger.warn(req.body.email+" is try to login with invalid credentials");
				res.send(user);
			}
		}
			
	})
})

app.get("/authenticateRequest/:token",function(req,res){
	var currToken=req.params.token;
	console.log(currToken);
	console.log(token.length);
	console.log(secrets.length);
	var flag=0;
	if(token.length>0)
	{
		try{
			for(var i=0;i<token.length;i++)
			{
				if(token[i]==currToken)
				{
					flag=1;
					var decoded = jwt.decode(currToken, secrets[i]);
					console.log("authorized");
					res.send("authorized");
			
				}
				
			}
			if(flag==0)
			{
				logger.error(req.body.email+" is not an authorized user");
				res.send("unauthorized");
			}
			
		}
		catch(err){
			logger.error(req.body.email+" is not an authorized user");
			res.send("unauthorized");
		}
	}
	else
	{
		logger.error(req.body.email+" is not an authorized user");
		res.send("unauthorized");
	}
	
})

app.post('/logout',function(req,res)
{
	var email = req.body._id;
	for(var i=0;i<token.length;i++){
		if(token[i]!=undefined && secrets[i]!=undefined){
		
			if(jwt.decode(token[i], secrets[i]).email==req.body._id)
			{
				token.splice(i,1);
				secrets.splice(i,1);
				break;
			}
		}
	}
	logger.info(email+" have been successfully logged out")
	res.send("You have been successfully logged out");
})

app.post('/getMyOrders', function(req,res){
	var email = req.body._id;	
	order.find({user : email}, function(err,orderList){
		if(err) {
			logger.error("Error occurred when "+email+" is try to fetch Order details");
			res.send(err);
		}else if (orderList!=null) {
			logger.info("Order details of "+email+" are successfully fetched");
			res.send(orderList);
		}else {
			logger.info(email+" have no order to fetch");
			res.send("No orders");
		}
	})
})

app.post('/getOrders', function(req, res){
    var order_id = req.body; 
     //order_id = {"_id":"1111121"};	
     console.log(order_id);	
    order.find(order_id, function(err,orderList){
        if(err) {
            logger.error("Error occurred when "+order_id+" is try to fetch Order details");
            res.send(err);
        }else if (orderList!=null) {
            logger.info("Order details of "+order_id+" are successfully fetched");
			console.log(orderList);
            res.send(orderList);
        }else {
            logger.info(order_id+" have no order to fetch");
            res.send("No orders");
         }
    })
})

