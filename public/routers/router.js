
app.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider){

	$routeProvider.
	when('/login',{
		templateUrl:'../pages/login.html'
	})
	.when('/track',{
		templateUrl:'../pages/track.html'
	})
	.when('/home',{
		templateUrl:'../pages/home.html'
	})
	.when('/consignment',{
		templateUrl:'../pages/consignment.html'
	})
	.otherwise({
        templateUrl:'../pages/login.html'
    });
	
}])
