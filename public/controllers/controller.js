'use strict';

var app=angular.module('app', ['ngRoute','ngStorage']);


app.controller('MainController', ['$scope', '$http', '$rootScope', '$timeout', '$q','$window','$location', '$sessionStorage',function($scope, $http, 
$rootScope, $timeout, $q, $window,$location, $sessionStorage) {

	$http.defaults.cache = false;
	
	if (!$http.defaults.headers.get) {
      $http.defaults.headers.get = {};
    }
	
	
	
	$scope.loginPage=function(){
		$window.sessionStorage["userInfo"]!=undefined;
		$rootScope.me=undefined;
		$scope.logout();

		if($window.sessionStorage["userInfo"]!="undefined" && $window.sessionStorage["userInfo"]!=undefined && 
$window.sessionStorage["userInfo"]!="{}")
			JSON.parse($window.sessionStorage["userInfo"]).token="first";
		
	}
	$scope.user={};
	$rootScope.me={};
	$scope.login = function(){
		$rootScope.sessMsg="";
		var deferred = $q.defer();
		$rootScope.message=undefined;
		$rootScope.logOutMsg=undefined;
		$http.post('/login',$scope.user).success(function(res){
				if(res!="")
				{
					var userInfo={
						token:res.token,
						currentUser: JSON.stringify(res.user)
					}
					$window.sessionStorage["userInfo"]=JSON.stringify(userInfo);
					deferred.resolve(userInfo);
					$rootScope.me=JSON.parse(JSON.parse($window.sessionStorage["userInfo"]).currentUser);
					$location.path("/home");
					$scope.getOrder();
					$scope.orders= undefined;
					$sessionStorage.orderNo = undefined;
					$sessionStorage.orders = undefined;
					$sessionStorage.orders = {};
					$scope.orders= {};
				}
				else
				{
					$rootScope.message="Invalid Username/Password. Please try again";
				}
		})
		$http.post('/login',$scope.user).error(function(err){
			console.log("Some technical issue");
		})		
	}
	
	$scope.getOrderNo=function(no) {
		$sessionStorage.orderNo = no;
		if ($location.$$path=="/track") {
			$scope.getTrackDetails();
		}
	}
	
	$scope.trackId="";
	$scope.getSta=function(trackId) {
		$scope.trackId=trackId;
		console.log($scope.trackId)
		$scope.getTrackDetail();
		
	}
	
	$scope.logout=function(){
		if($window.sessionStorage["userInfo"]!=undefined && $window.sessionStorage["userInfo"]!="undefined")
		{
			console.log(JSON.parse(JSON.parse($window.sessionStorage["userInfo"]).currentUser));
			$http.post("/logout",JSON.parse(JSON.parse($window.sessionStorage["userInfo"]).currentUser)).success(function(res){
				$window.sessionStorage["userInfo"]=undefined;
				$rootScope.me=undefined;
				$location.path("/login");
				$rootScope.logOutMsg="You have been successfully logged out";
				$rootScope.message=undefined;
				$sessionStorage.orderNo = undefined;
				$sessionStorage.orders = undefined;
			})
		}
	}
	
	$scope.value = undefined;
	
	$rootScope.logOutMsg="";
	
	$scope.authenticateReq=function(){
		if($window.sessionStorage["userInfo"]!=undefined && $window.sessionStorage["userInfo"]!="undefined" && $window.sessionStorage["userInfo"]!=undefined){
			$http.get("/authenticateRequest/"+JSON.parse($window.sessionStorage["userInfo"]).token).success(function(res){
				if(res=="unauthorized")
				{
					$rootScope.me=undefined;
					$window.sessionStorage["userInfo"]=undefined;
					$rootScope.logOutMsg=undefined;
					$scope.value = "unauthorized";
				}
				else
				{
					$rootScope.me=JSON.parse(JSON.parse($window.sessionStorage["userInfo"]).currentUser);
					$scope.value = "authorized";
				}
			})
		}
	}
	

	var checkAuth=function(){
		if($window.sessionStorage["userInfo"]!=undefined && $window.sessionStorage["userInfo"]!="undefined" && 
$window.sessionStorage["userInfo"]!="{}"){
			if($location.$$path=="/login"){
				$http.post("/logout",JSON.parse(JSON.parse($window.sessionStorage["userInfo"]).currentUser)).success(function(res){
					$window.sessionStorage["userInfo"]=undefined;
					$rootScope.me=undefined;
					$location.path("/login");
				})
			}
			else
				$rootScope.loggedIn=true;
		}
		else
		{
				$rootScope.loggedIn=false;
				if($location.$$path!="/login" && $location.$$path!="/home")
				{
					$location.path("/login");
					
				}
		}	
	}
	
	
	$scope.myOrders=[];
	//checkAuth();
	
	$scope.getOrder = function() {
		var flag=false;
		var index = 0;
		if($window.sessionStorage["userInfo"]!=undefined && $window.sessionStorage["userInfo"]!="undefined" ){
			$rootScope.me=JSON.parse(JSON.parse($window.sessionStorage["userInfo"]).currentUser);
			$http.post("/getMyOrders",JSON.parse(JSON.parse($window.sessionStorage["userInfo"]).currentUser)).success(function(res){
				if (res!=null && res!=undefined){
					$sessionStorage.orders = res;
					angular.forEach($sessionStorage.orders, function(order){
						if (order!=null && order!=undefined && order.length!=0){
							angular.forEach(order.lineDelivery, function(delivery){
								if(delivery!=null && delivery!=undefined && delivery.length!=0){
									angular.forEach(delivery.item, function(item){
										if(item!=null && item!=undefined){
											if(item.itemId!=null && item.itemId!=undefined){
												angular.forEach(item.itemId.status, function(state){
													if(state!=null && state!=undefined){
														if (state.status=='Delivered'){
															flag = true;
														}
													}
												})
											}
										}
										if (flag) {
											var orderId = order._id+"#"+delivery.deliveryNo;
											$scope.myOrders[index] = {'Delivered' : true, 'orderId' : orderId};
											index++;
										}else {
											var orderId = order._id+"#"+delivery.deliveryNo;
											$scope.myOrders[index] = {'Delivered' : false, 'orderId' : orderId};
											index++;
										}flag=false;
										
									})
								}
							})
						}
					})
				}
			});
		}
	}
	$scope.getOrder();
	
	
	$scope.isEmpty = function (obj) {
		for (var i in obj) if (obj.hasOwnProperty(i)) return false;
		return true;
	}
	
	$scope.trackingDetails= [];
	
	$scope.trackMessage="";
	
	$scope.getTrackDetails =function() {
		$scope.getOrder();
		var index=0;
		if($window.sessionStorage["userInfo"]!=undefined && $window.sessionStorage["userInfo"]!="undefined" ){
			if ($sessionStorage.orderNo!=undefined || $sessionStorage.orderNo!=null && $sessionStorage.orderNo!="") {
				var orderArray = $sessionStorage.orderNo.split("#");
				if(!$scope.isEmpty($sessionStorage.orders)) {
					angular.forEach($sessionStorage.orders, function(order){
						if (order!=null && order!=undefined && order.length!=0){
							if (order._id==orderArray[0]) {
								angular.forEach(order.lineDelivery, function(delivery){
									if(delivery!=null && delivery!=undefined && delivery.length!=0){
										if (delivery.deliveryNo==orderArray[1]) {
											angular.forEach(delivery.item, function(item){
												if(item!=null && item!=undefined){
													if(item.itemId!=null && item.itemId!=undefined){
														angular.forEach(item.itemId.status, function(state){
															$scope.trackingDetails[index]=item;
															index++;
														})
													//	if (item.itemId==orderArray[2]) {
													//		angular.forEach(item.itemId.status, function(state){
													//			$scope.trackingDetails[index]=state;
													//			index++;
													//		})
													//	}
													}
												}
											})
										}
									}
								})
							}
						}
					})
				}
				if($scope.isEmpty($scope.trackingDetails)){
					$scope.trackMessage = "Please enter a valid Track Id";
				}
			}
		}
	}
	$scope.myStatus=[];

	$scope.getStatus = function(){
		var index = 0;
		var FormData = {
		        '_id': $scope.trackId  
		    }
	    $http({method: 'POST', url: '/getOrders', data:FormData}).success(function(res) {
			if(res!=null && res!=undefined){
				$scope.orders=res;
				if(!$scope.isEmpty($scope.trackingDetail)) {
					$scope.trackingDetail=[];
				}		
				angular.forEach($scope.orders, function(order){
					if (order!=null && order!=undefined && order.length!=0){
						angular.forEach(order.lineDelivery, function(delivery){
							if(delivery!=null && delivery!=undefined && delivery.length!=0){
								angular.forEach(delivery.item, function(item){
									if(item!=null && item!=undefined){
										if(item.itemId!=null && item.itemId!=undefined){
											angular.forEach(item.itemId.status, function(state){
												console.log(item.itemId.status);
												if(state!=null && state!=undefined){
													$scope.trackingDetail[index]=state;
													index++;													
												}
											})
										}
									}																		
								})
							}
						})
					}
				})
			}
		}).error(function(res) {
	        console.log('error');
	    });
		console.log($scope.trackingDetail);
	}
	
	$scope.trackingDetail= [];
	$scope.getTrackDetail =function() {
		if ($scope.trackId!=undefined || $scope.trackId!=null && $scope.trackId!="") {
			$scope.trackMessage="";
			if(!$scope.isEmpty($scope.trackId)) {
				$scope.getStatus();
			}
			if($scope.isEmpty($scope.trackId)) {
				$scope.trackMessage="Please enter a valid Track Id";
			}
		}		
		}
		if ($scope.trackId==undefined || $scope.trackId==null && $scope.trackId=="") {
			$scope.trackMessage = "Please enter a valid Tracking Id";
		}
		if ($scope.trackId==undefined || $scope.trackId==null && $scope.trackId==""|| $scope.trackingDetail==undefined) {
			$scope.trackMessage = "Please enter a valid Tracking Id";
		}
	
	$scope.trackId={'track' : ''};
	$scope.showOptions=function(string){
		var keys=[];
		$scope.trackData=[];
		angular.forEach($scope.myOrders, function(ord){
			var key = ord.orderId;
			if (key.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
				if (keys.indexOf(key) === -1) {
					keys.push(key);
				}
			}
		});
		$scope.hide=false;
		$scope.trackData = keys;
	}
	$scope.filterTrack= function(string) {
		$scope.trackId.track=string;
		$sessionStorage.orderNo=string;
		$scope.hide=true;
	}
}]);